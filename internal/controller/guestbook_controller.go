/*
Copyright 2023 KubeSec.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	webappv1 "gitee.com/KubeSec/guestbook/api/v1"
)

// GuestbookReconciler reconciles a Guestbook object
type GuestbookReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	// See that we added the following code to allow us to pass the record.EventRecorder
	Recorder record.EventRecorder
}

//+kubebuilder:rbac:groups=webapp.gitee.com,resources=guestbooks,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=webapp.gitee.com,resources=guestbooks/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=webapp.gitee.com,resources=guestbooks/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=pods,verbs=create;get;list;watch;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=events,verbs=create;patch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Guestbook object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *GuestbookReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	// TODO(user): your logic here
	guestBook := &webappv1.Guestbook{}
	if err := r.Client.Get(ctx, req.NamespacedName, guestBook); err != nil {
		return ctrl.Result{}, err
	}

	// 表示已请求删除
	if !guestBook.DeletionTimestamp.IsZero() {
		err := r.Client.Delete(ctx, &corev1.Pod{
			TypeMeta: metav1.TypeMeta{
				APIVersion: corev1.SchemeGroupVersion.String(),
				Kind:       "Pod",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      guestBook.Spec.Name,
				Namespace: guestBook.Namespace,
			},
		})
		if err != nil {
			return ctrl.Result{}, err
		}
		guestBook.Finalizers = []string{}
		if err := r.Update(ctx, guestBook); err != nil {
			return ctrl.Result{}, err
		}
	}

	pod := &corev1.Pod{
		TypeMeta: metav1.TypeMeta{
			APIVersion: corev1.SchemeGroupVersion.String(),
			Kind:       "Pod",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      guestBook.Spec.Name,
			Namespace: guestBook.Namespace,
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name: "nginx",
					Ports: []corev1.ContainerPort{
						{
							ContainerPort: 80,
							Name:          "http",
							Protocol:      "TCP",
						},
					},
					Image: guestBook.Spec.Image,
				},
			},
		},
	}
	if err := r.Create(ctx, pod); err != nil {
		return ctrl.Result{}, err
	}
	r.Recorder.Event(guestBook, corev1.EventTypeNormal, "Created", fmt.Sprintf("Custom Resource %s is being deleted from the namespace %s", guestBook.Name, guestBook.Namespace))

	guestBook.Finalizers = append(guestBook.Finalizers, guestBook.Spec.Name)
	if err := r.Update(ctx, guestBook); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *GuestbookReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&webappv1.Guestbook{}).
		Complete(r)
}
